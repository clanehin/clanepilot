
SAS OFF.

// ######################################################################
//    LANDING LEGS
// ######################################################################

IF SLOW_TIMING_EVENT {

  IF ALT:RADAR + SHIP:VERTICALSPEED*7 - VESSEL_SIZE < 0 {
    LEGS ON.
    GEAR ON.
    IF SHIP:VELOCITY:SURFACE:MAG > 1.0 {
      RCS ON.
    } ELSE {
      RCS OFF.
    }
  }
  ELSE IF ALT:RADAR + SHIP:VERTICALSPEED*10 > 250 {
    LEGS OFF.
    GEAR OFF.
    RCS OFF.
  }

  IF INLIGHT {
    LIGHTS OFF.
  } ELSE {
    LIGHTS ON.
  }

  IF ALT:PERIAPSIS > 0 AND SHIP:SENSORS:PRES < 0.01 AND SHIP:PARTSTAGGED("Fairing"):LENGTH <= 0 {
    PANELS ON.
  }
  ELSE {
    PANELS OFF.
  }
}

IF ALT:RADAR < 150 + VESSEL_SIZE AND SHIP:VERTICALSPEED < -50 {
  CHUTES ON.
}
